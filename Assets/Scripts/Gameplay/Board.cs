namespace Gameplay
{
    public struct Board
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int[] IndexGrid { get; private set; }
        public BoardItem<BoardItemBase>[] ItemGrid { get; private set; }

        public Board(int width, int height, int[] indexGrid, BoardItem<BoardItemBase>[] itemGrid)
        {
            Width = width;
            Height = height;
            IndexGrid = indexGrid;
            ItemGrid = itemGrid;
        }
    }
}
using UnityEngine;

namespace Gameplay
{
    public class BoardItem<T> : MonoBehaviour where T : BoardItemBase
    {
        public T Item { get; private set; }
        
        public BoardItem(T item)
        {
            Item = item;
        }
    }
}
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Gameplay
{
    public class Drop : BoardItemBase, IMovable, ICollectible
    {
        public Drop(BoardItemBase item) : base(item)
        {
        }
        
        public async Task MoveToPosition(Vector3 targetPosition, float duration, YieldAwaitable yield)
        {
            var elapsedTime = 0f;
            var transformToUse = transform;
            var initialPosition = transformToUse.position;
        
            while (elapsedTime < duration)
            {
                transformToUse.position = Vector3.Lerp(initialPosition, targetPosition, elapsedTime / duration);
                elapsedTime += Time.deltaTime;
                
                await yield;
            }
            transformToUse.position = targetPosition;
        }

        public async Task CollectItem(Action<Drop> onCollectedAction, YieldAwaitable yield)
        {
            var elapsedTime = 0f;
            var transformToUse = this.transform;
            var initialScale = transformToUse.localScale;
        
            while (elapsedTime < 0.2f)
            {
                transform.localScale = Vector3.Lerp(initialScale, Vector3.zero, elapsedTime / 0.2f);
                elapsedTime += Time.deltaTime;
        
                await yield;
            }
            transformToUse.localScale = Vector3.zero;
            onCollectedAction?.Invoke(this);
        }
        
        public void ResetScale()
        {
            transform.localScale = Vector3.one;
        }
        
        public Transform GetTransform()
        {
            return transform;
        }
    }
}

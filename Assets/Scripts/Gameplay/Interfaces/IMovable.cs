using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Gameplay
{
    public interface IMovable
    {
        Task MoveToPosition(Vector3 targetPosition, float duration, YieldAwaitable yield);
        Transform GetTransform();
    }
}

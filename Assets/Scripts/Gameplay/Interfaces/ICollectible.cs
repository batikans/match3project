using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Gameplay
{
    public interface ICollectible
    {
        Task CollectItem(Action<Drop> onCollectedAction, YieldAwaitable yield);
    }
}

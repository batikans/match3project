using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor
{
    public static class AutoSceneLoader
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void StartGameScene()
        {
            var scenes = EditorBuildSettings.scenes;
            if (scenes.Length > 0)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
using System.Threading.Tasks;
using Gameplay;

namespace Management
{
    public class MatchController : ControllerBase, IController
    {
        private BoardStateController _boardStateController;
        
        private int _width;
        private int _height;
        
        public async Task InitializeController()
        {
            _width = BoardSettings.Width;
            _height = BoardSettings.Height;
            _boardStateController = ControllersManager.BoardStateController;
            
            await Task.Yield();
        }
        
        internal bool GetIsAnyMatchExistsOnBoard()
        {
            var board = _boardStateController.GameBoard;
            return GetIsAnyHorizontalMatchExists(board) || GetIsAnyVerticalMatchExists(board);
        }
    
        private bool GetIsAnyHorizontalMatchExists(Board board)
        {
            var grid = board.IndexGrid;

            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width - 2; x++)
                {
                    var index = BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, _width);
                    var dropIndexToCompare = grid[index];
                    if(dropIndexToCompare == -1) continue;

                    if (dropIndexToCompare == grid[index + 1] && dropIndexToCompare == grid[index + 2])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool GetIsAnyVerticalMatchExists(Board board)
        {
            var grid = board.IndexGrid;

            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height - 2; y++)
                {
                    var index = x + y * _width;
                    var dropIndexToCompare = grid[index];
                    if(dropIndexToCompare == -1) continue;
                    
                    if (dropIndexToCompare == grid[index + _width] && dropIndexToCompare == grid[index + 2 * _width])
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public bool GetIsAnyPotentialMatchExist()
        {
            var board = _boardStateController.GameBoard;
            
            for (var i = 0; i < board.IndexGrid.Length - 1; i++)
            {
                for (var j = i + 1; j < board.IndexGrid.Length; j++)
                {
                    if (GetIsPotentialMatch(board, i, j))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private bool GetIsPotentialMatch(Board board, int index1, int index2)
        {
            var areAdjacent = BoardUtilities.GetAreItemsAdjacent(board.Width, index1, index2);
            if (!areAdjacent) return false;
            
            var indexGrid = board.IndexGrid;
            if (indexGrid[index1] == -1 || indexGrid[index2] == -1) return false;
            
            BoardUtilities.SwapItemsOnBoard(board, index1, index2);
            var matchExists = GetIsAnyMatchExistsOnBoard();
            BoardUtilities.SwapItemsOnBoard(board, index1, index2);
        
            return matchExists;
        }
    }
}
using UnityEngine;

namespace Management
{
    public class ControllerBase : MonoBehaviour
    {
        protected internal ControllersManager ControllersManager;
        protected internal BoardSettings BoardSettings;

        protected virtual void Awake()
        {
            ControllersManager = GetComponentInParent<ControllersManager>();
            BoardSettings = ControllersManager.BoardSettings;
        }
    }
}

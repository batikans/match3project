using System.Threading.Tasks;
using UnityEngine;

namespace Management
{
    public class InputController : ControllerBase, IController
    {
        private BoardStateController _boardStateController;
        
        private InputState _inputState = InputState.Inactive;
        public Camera gameCamera;

        private Vector3 _mouseDownPosition;
        private Vector2Int _onTouchCoordinate;
        private Vector2Int _onSwipeCoordinate;

        internal int TouchStartGridIndex;
        internal int SwipeGridIndex;
        private int _touchStartItemID;
        private int _swipeItemID;
        
        private bool _canReceiveInputInfo;

        protected override void Awake()
        {
            base.Awake();

            _boardStateController = ControllersManager.BoardStateController;
            Input.multiTouchEnabled = false;
        }

        public async Task InitializeController()
        {
            _canReceiveInputInfo = true;
            gameCamera = Camera.main;
            
            await Task.Yield();
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                _canReceiveInputInfo = true;
            }
            
            if (_inputState == InputState.Inactive) return;
            
            _mouseDownPosition = gameCamera.ScreenToWorldPoint(Input.mousePosition);
            
            if (Input.GetMouseButtonDown(0))
            {
                _onTouchCoordinate = BoardUtilities.GetMousePositionToCoordinate(_mouseDownPosition);
                _onSwipeCoordinate = _onTouchCoordinate;
            }
            else if (Input.GetMouseButton(0))
            {
                if (!_canReceiveInputInfo) return;
                
                _onSwipeCoordinate = BoardUtilities.GetMousePositionToCoordinate(_mouseDownPosition);
                
                if (_onTouchCoordinate == _onSwipeCoordinate) return;
                _canReceiveInputInfo = false;
                _inputState = InputState.Inactive;
                
                AnalyzeInput();
            }
        }
        
        internal void ToggleInput(bool isActive)
        {
            _inputState = isActive ? InputState.Active : InputState.Inactive;
        }
        
        private void AnalyzeInput()
        {
            var width = BoardSettings.Width;
            var height = BoardSettings.Height;
        
            var isTouchStartInsideBoardZone = BoardUtilities.GetIsTouchInsideBoardZone(_onTouchCoordinate, width, height);
            var isSwipeInsideBoardZone = BoardUtilities.GetIsTouchInsideBoardZone(_onSwipeCoordinate, width, height);
        
            if (!isTouchStartInsideBoardZone || !isSwipeInsideBoardZone) goto ActivateInput;
            
            TouchStartGridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(_onTouchCoordinate.x, _onTouchCoordinate.y, width);
            SwipeGridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(_onSwipeCoordinate.x, _onSwipeCoordinate.y, width);

            var indexGrid = _boardStateController.GameBoard.IndexGrid;
            _touchStartItemID = indexGrid[TouchStartGridIndex];
            _swipeItemID = indexGrid[SwipeGridIndex];
            
            var itemIndexCheck = _touchStartItemID < 0 || _swipeItemID < 0;
            var adjacentCheck = !BoardUtilities.GetAreItemsAdjacent(_onTouchCoordinate, _onSwipeCoordinate);
            
            if (itemIndexCheck || adjacentCheck) goto ActivateInput;
        
            _boardStateController.BoardStateMachine.ChangeState(_boardStateController.ItemSwapState);
            return;
            
            ActivateInput:
            ToggleInput(true);
        }
    }
}
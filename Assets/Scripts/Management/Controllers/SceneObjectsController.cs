using System.Threading.Tasks;
using UnityEngine;

namespace Management
{
    public class SceneObjectsController : ControllerBase, IController
    {
        public async Task InitializeController()
        {
            await Task.WhenAll(
                InitializeCamera(),
                InitializeTiles(),
                InitializeBoardMask());
        }
        
        private async Task InitializeCamera()
        {
            var assetReference = BoardSettings.CameraReference;
            var loadedAsset = await assetReference.LoadAssetAsync<GameObject>().Task;
            var gameCamera = Instantiate(loadedAsset).GetComponent<Camera>();

            var screenRatioMultiplier = BoardSettings.ScreenRatioMultiplier;
            
            float orthographicSize;
            if (gameCamera.aspect > 0.8)
            {
                orthographicSize = 5.5f;
            }
            else
            {
                orthographicSize = screenRatioMultiplier * Screen.height / Screen.width * 0.5f;
            }
            
            gameCamera.orthographicSize = orthographicSize;
            gameCamera.transform.position = new Vector3(BoardSettings.Width  * 0.5f - 0.5f, 4, -10);
            
            assetReference.ReleaseAsset();
        }
        
        private async Task InitializeBoardMask()
        {
            var assetReference = BoardSettings.BoardMaskReference;
            var loadedAsset = await assetReference.LoadAssetAsync<GameObject>().Task;
            var boardMask = Instantiate(loadedAsset);
            var boardMaskTransform = boardMask.transform;
            var width = BoardSettings.Width;
            var height = BoardSettings.Height;
            
            boardMaskTransform.localScale = new Vector3(width, height, 1);
            boardMaskTransform.position = new Vector3(width * 0.5f - 0.5f,
                (float)height * 0.5f - 0.5f, -1f);
            
            assetReference.ReleaseAsset();
        }
        
        private async Task InitializeTiles()
        {
            var assetReference = BoardSettings.TileReference;
            var loadedAsset = await assetReference.LoadAssetAsync<GameObject>().Task;
        
            for (int x = 0; x < BoardSettings.Width; x++)
            {
                for (int y = 0; y < BoardSettings.Height; y++)
                {
                    var tile = Instantiate(loadedAsset,
                        new Vector3(x, y, 0), Quaternion.identity, transform);
#if UNITY_EDITOR
                    tile.name = $"Tile_{x},{y}";
#endif
                }
            }
            assetReference.ReleaseAsset();
        }
    }
}

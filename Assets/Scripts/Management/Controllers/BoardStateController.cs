using System.Collections.Generic;
using System.Threading.Tasks;
using Gameplay;
using UnityEngine;
using UnityEngine.Profiling;

namespace Management
{
    public class BoardStateController : ControllerBase, IController
    {
        internal BoardStateMachine BoardStateMachine { get; private set; }
        private InitializeBoardIndicesState InitializeBoardIndicesState { get; set; }
        internal InitializeBoardItemsState InitializeBoardItemsState { get; private set; }
        internal FillEmptyTilesState FillEmptyTilesState { get; private set; }
        internal InputReceiveState InputReceiveState { get; private set; }
        internal DestroyItemsState DestroyItemsState { get; private set; }
        internal ShuffleBoardState ShuffleBoardState { get; private set; }
        internal MoveItemsState MoveItemsState { get; private set; }
        internal ItemSwapState ItemSwapState { get; private set; }
        
        internal Board GameBoard;
        
        internal List<int> ActiveSpawnerIndices;
        internal List<Vector2Int> MatchedItems;

        internal int TotalDropsCount;
        internal bool IsCurrentMatchExist;
        internal bool IsPotentialMatchExist;
        
        public async Task InitializeController()
        {
            SetupBoard();
            InitializeActiveSpawners();
            InitializeBoardStateMachine();
            
            await Task.CompletedTask;
        }
    
        private void SetupBoard()
        {
            var width = BoardSettings.Width;
            var height = BoardSettings.Height;
            
            var arraySize = width * height;
            GameBoard = new Board(width, height, new int[arraySize], 
                new BoardItem<BoardItemBase>[arraySize]);
            MatchedItems = new List<Vector2Int>();
            TotalDropsCount = BoardSettings.DropReferences.Length;
        }

        private void InitializeActiveSpawners()
        {
            ActiveSpawnerIndices = new List<int>();
            var spawnerTypes = BoardSettings.SpawnerTypes;
            var spawnersCountOnBoardSettings = spawnerTypes.Length;
            
            for (int i = 0; i < BoardSettings.Width; i++)
            {
                var spawnerType = i < spawnersCountOnBoardSettings ? spawnerTypes[i] : SpawnerType.Active;
                if(spawnerType == SpawnerType.Active) ActiveSpawnerIndices.Add(i);
            }
        }
        
        private void InitializeBoardStateMachine()
        {
            BoardStateMachine = new BoardStateMachine();

            InitializeBoardIndicesState = new InitializeBoardIndicesState(this);
            InitializeBoardItemsState = new InitializeBoardItemsState(this);
            FillEmptyTilesState = new FillEmptyTilesState(this);
            InputReceiveState = new InputReceiveState(this);
            DestroyItemsState = new DestroyItemsState(this);
            ShuffleBoardState = new ShuffleBoardState(this);
            MoveItemsState = new MoveItemsState(this);
            ItemSwapState = new ItemSwapState(this);
        }
        
        internal void StartStateMachine()
        {
            BoardStateMachine.Initialize(InitializeBoardIndicesState);
        }
    }
}

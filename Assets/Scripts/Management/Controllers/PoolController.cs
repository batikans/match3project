using System.Collections.Generic;
using System.Threading.Tasks;
using Gameplay;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Management
{
    public class PoolController : ControllerBase, IController
    {
        private Dictionary<int, Drop> _loadedDropReferences;
        private Dictionary<int, Queue<Drop>> _dropPoolDictionary;

        public async Task InitializeController()
        {
            await InitializePool();
        }
    
        private async Task InitializePool()
        {
            _dropPoolDictionary = new Dictionary<int, Queue<Drop>>();
            _loadedDropReferences = new Dictionary<int, Drop>();
            
            var assetReferences = BoardSettings.DropReferences;
            var poolSize = BoardSettings.PoolSize;
            var count = assetReferences.Length;
            var tasks = new Task[count];

            for (int i = 0; i < count; i++)
            {
                var assetReference = assetReferences[i];
                var task = CreatePool(assetReference, poolSize, i);
                tasks[i] = task;
            }

            await Task.WhenAll(tasks);
        }
    
        private async Task CreatePool(AssetReference assetReference, int poolSize, int id)
        {
            var dropPool = new Queue<Drop>(poolSize);

            var loadedAsset = await assetReference.LoadAssetAsync<GameObject>().Task;
            var loadedDropReference = loadedAsset.GetComponent<Drop>();
            
            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool(dropPool, loadedDropReference);
            }
            
            _loadedDropReferences.Add(id, loadedDropReference);
            _dropPoolDictionary.Add(id, dropPool);
        }
    
        private void AddObjectToPool(Queue<Drop> dropPool, Drop loadedDropReference)
        {
            var objectPrefab = Instantiate(loadedDropReference, this.transform, true);
            objectPrefab.gameObject.SetActive(false);
            dropPool.Enqueue(objectPrefab);
        }

        public Drop GetFromPool(int id)
        {
            if (!_dropPoolDictionary.TryGetValue(id, out var dropPool)) return null;
            if (dropPool.Count <= 0)
            {
                if (!_loadedDropReferences.TryGetValue(id, out var loadedDropReference)) return null;
                AddObjectToPool(dropPool, loadedDropReference);
            }
        
            var objectToSpawn = dropPool.Dequeue();
            objectToSpawn.ResetScale();
            objectToSpawn.gameObject.SetActive(true);
            return objectToSpawn;
        }

        public void ReturnToPool(Drop dropToReturn, int id)
        {
            _dropPoolDictionary[id].Enqueue(dropToReturn);
            dropToReturn.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            var dropReferences = BoardSettings.DropReferences;
            for (int i = 0; i < dropReferences.Length; i++)
            {
                dropReferences[i].ReleaseAsset();
            }
        }
    }
}

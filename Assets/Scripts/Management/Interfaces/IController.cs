using System.Threading.Tasks;

namespace Management
{
    public interface IController
    {
        Task InitializeController();
    }
}

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Serialization;

namespace Management
{
    [CreateAssetMenu(menuName = "Board Settings")]
    public class BoardSettings : ScriptableObject
    {
        [Header("Application Settings")]
        [SerializeField] private int targetFrameRate = 60;
        [SerializeField, Tooltip("Adjusts similar view angle on different aspect ratios")] 
        private float screenRatioMultiplier = 8.4f;
        public int TargetFrameRate => targetFrameRate;
        public float ScreenRatioMultiplier => screenRatioMultiplier;
    
        [Header("Object Pool Settings")]
        [SerializeField, Tooltip("Initial objects amount to spawn for each object pool")] 
        private int poolSize = 32;
        public int PoolSize => poolSize;
    
        [Header("Match Settings")]
        [SerializeField] [Range(5, 8)] private int height = 8;
        [SerializeField] [Range(5, 8)] private int width = 8;
        [SerializeField, Tooltip("Set spawner availability for each row")] 
        private SpawnerType[] spawnerTypes;
        public int Height => height;
        public int Width => width;
        public SpawnerType[] SpawnerTypes => spawnerTypes;

        [Header("Asset References of Board Entities")] 
        [SerializeField] private AssetReference[] dropReferences;
        [SerializeField] private AssetReference tileReference;
        [SerializeField] private AssetReference boardMaskReference;
        [SerializeField] private AssetReference cameraReference;
        public AssetReference[] DropReferences => dropReferences;
        public AssetReference TileReference => tileReference;
        public AssetReference BoardMaskReference => boardMaskReference;
        public AssetReference CameraReference => cameraReference;
        
        [FormerlySerializedAs("moveSpeedMultiplier")]
        [Header("Object Movement")]
        [SerializeField][Range(1, 15)] private float moveSpeed = 6.25f;
        [SerializeField][Range(1, 15)] private float shuffleMoveSpeed = 12f;
        public float MoveSpeed => moveSpeed;
        public float ShuffleMoveSpeed => shuffleMoveSpeed;
        
        private void OnValidate()
        {
            if (spawnerTypes.Length == width) return;

            var copyOfSpawnerTypes = spawnerTypes;
            var oldArrayCount = spawnerTypes.Length;
            
            spawnerTypes = new SpawnerType[width];
            
            for (int i = 0; i < width; i++)
            {
                spawnerTypes[i] = i < oldArrayCount ? copyOfSpawnerTypes[i] : SpawnerType.Active;
            }
        }
    }
}
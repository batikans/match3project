using UnityEngine;

namespace Management
{
    public class SceneInitializer : MonoBehaviour
    {
        [SerializeField] private BoardSettings boardSettings;

        private void Awake()
        {
            Application.targetFrameRate = boardSettings.TargetFrameRate;
        }

        private async void Start()
        {
            var controllersManagerObject = new GameObject {name = "ControllersManager"};
            var controllersManager = controllersManagerObject.AddComponent<ControllersManager>();
            
            await controllersManager.Initialize(boardSettings);
        }
    }
}

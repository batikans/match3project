using Gameplay;
using UnityEngine;
using Random = System.Random;

namespace Management
{
    public static class BoardUtilities
    {
        private static readonly Random Random = new Random();
        public static int GetRandomDropIndex(int totalDropsCount)
        {
            var randomIndex = Random.Next(0, totalDropsCount);
        
            return randomIndex;
        }
        
        public static int GetGridIndexBasedOnCoordinates(int x, int y, int width)
        {
            return x + y * width;
        }

        public static bool GetIsTouchInsideBoardZone(Vector2Int mousePosition, int width, int height)
        {
            var positionX = mousePosition.x;
            var positionY = mousePosition.y;
        
            var verticalCheck = positionX >= 0 && positionX < width;
            var horizontalCheck = positionY >= 0 && positionY < height;

            return verticalCheck && horizontalCheck;
        }
        
        
        public static bool GetAreItemsAdjacent(int width, int index1, int index2)
        {
            if (index1 == index2)
                return false;
        
            var item1Y = index1 / width;
            var item1X = index1 % width;

            var item2Y = index2 / width;
            var item2X = index2 % width;

            if (item1Y == item2Y && Mathf.Abs(item1X - item2X) == 1)
                return true;

            return item1X == item2X && Mathf.Abs(item1Y - item2Y) == 1;
        }
        
        public static bool GetAreItemsAdjacent(Vector2Int touchStartPosition, Vector2Int swipePosition)
        {
            if (touchStartPosition.y == swipePosition.y &&
                Mathf.Abs(touchStartPosition.x - swipePosition.x) == 1)
                return true;

            return touchStartPosition.x == swipePosition.x &&
                   Mathf.Abs(touchStartPosition.y - swipePosition.y) == 1;
        }

        internal static void SwapItemsOnBoard(Board board, int index1, int index2)
        {
            var indexGrid = board.IndexGrid;
            var itemGrid = board.ItemGrid;
            
            (indexGrid[index1], indexGrid[index2]) = (indexGrid[index2], indexGrid[index1]);
            (itemGrid[index1], itemGrid[index2]) = (itemGrid[index2], itemGrid[index1]);
        }
        
        public static Vector2Int GetMousePositionToCoordinate(Vector2 mousePosition)
        {
            var offset = 0.5f;
            var positionX = Mathf.FloorToInt (mousePosition.x + offset);
            var positionY = Mathf.FloorToInt(mousePosition.y + offset);

            return new Vector2Int(positionX, positionY);
        }
        
        public static void DebugBoardInfo(Board board)
        {
#if UNITY_EDITOR
            Debug.Log("-------------------------------------------");
            for (int y = board.Height - 1; y > -1; y--)
            {
                Debug.Log($"{board.IndexGrid[GetGridIndexBasedOnCoordinates(0, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(1, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(2, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(3, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(4, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(5, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(6, y, 8)]}   ,   " +
                          $"{board.IndexGrid[GetGridIndexBasedOnCoordinates(7, y, 8)]}");
            }
            
            Debug.Log("-------------------------------------------");
#endif
        }
    }
}
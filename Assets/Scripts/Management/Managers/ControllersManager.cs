using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace Management
{
    public class ControllersManager : MonoBehaviour
    {
        //Controllers should be listed in execution order
        internal PoolController PoolController;
        internal BoardStateController BoardStateController;
        internal SceneObjectsController SceneObjectsController;
        internal InputController InputController;
        internal MatchController MatchController;
        
        internal BoardSettings BoardSettings { get; private set; }
        
        public async Task Initialize(BoardSettings boardSettings)
        {
            BoardSettings = boardSettings;
            await InitializeControllers();
            
            BoardStateController.StartStateMachine();
        }

        private async Task InitializeControllers()
        {
            var controllerFields = GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            var controllers = new List<IController>();

            foreach (var field in controllerFields)
            {
                if (!typeof(IController).IsAssignableFrom(field.FieldType)) continue;

                var fieldType = field.FieldType;

                var controllerObject = new GameObject(fieldType.Name);
                controllerObject.transform.SetParent(this.transform);

                var controller = controllerObject.AddComponent(fieldType);
                field.SetValue(this, controller);

                controllers.Add((IController)controller);
            }

            //Initialize all controllers in execution order
            foreach (var controller in controllers)
            {
                await controller.InitializeController();
            }
        }
    }
}

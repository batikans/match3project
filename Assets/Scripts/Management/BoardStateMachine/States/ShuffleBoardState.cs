using System;

namespace Management
{
    public class ShuffleBoardState : BoardState
    {
        protected internal ShuffleBoardState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _random = new Random();
            _matchController = ControllersManager.MatchController;
        }

        private readonly MatchController _matchController;
        private readonly Random _random;
        
        protected internal override void EnterState()
        {
            base.EnterState();
            
            ShuffleBoardLogic();
        }
        
        private void ShuffleBoardLogic()
        {
            var board = BoardStateController.GameBoard;
            
            while (true)
            {
                for (int i = board.IndexGrid.Length - 1; i > 0; i--)
                {
                    if (board.IndexGrid[i] == -1) continue;
                    
                    var j = _random.Next(0, i + 1);
                    if (i == j || board.IndexGrid[j] == -1) continue;
                    
                    BoardUtilities.SwapItemsOnBoard(board, i, j);
                }
        
                if (_matchController.GetIsAnyMatchExistsOnBoard())
                    continue;
            
                if (!_matchController.GetIsAnyPotentialMatchExist())
                    continue;
                break;
            }
            BoardStateMachine.ChangeState(BoardStateController.MoveItemsState);
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Gameplay;
using UnityEngine;

namespace Management
{
    public class DestroyItemsState : BoardState
    {
        protected internal DestroyItemsState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _poolController = ControllersManager.PoolController;
            _tasks = new List<Task>();
        }

        private readonly PoolController _poolController;
        private readonly List<Task> _tasks;
        private readonly YieldAwaitable _yield = Task.Yield();

        protected internal override void EnterState()
        {
            base.EnterState();

            DestroyItemsLogic();
        }

        private async void DestroyItemsLogic()
        {
            var board = BoardStateController.GameBoard;
            BoardStateController.MatchedItems = GetMatchedItems(board.IndexGrid);
            
            await DestroyMatchedItems(board);
            
            BoardStateMachine.ChangeState(BoardStateController.FillEmptyTilesState);
        }
        
        private List<Vector2Int> GetMatchedItems(IReadOnlyList<int> indexGrid)
        {
            var matchedItems = BoardStateController.MatchedItems;
            matchedItems.Clear();
            
            var width = BoardSettings.Width;
            var height = BoardSettings.Height;
        
            //Vertical check
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width - 2; x++)
                {
                    var itemIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width);
                    var currentItem = indexGrid[itemIndex];

                    if (currentItem == -1) continue;
                    if (indexGrid[itemIndex + 1] != currentItem ||
                        indexGrid[itemIndex + 2] != currentItem) continue;

                    for (int i = 0; i < 3; i++)
                    {
                        matchedItems.Add(new Vector2Int(x + i, y));
                    }
                }
            }

            //Horizontal check
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height - 2; y++)
                {
                    var itemIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width);
                    var currentItem = indexGrid[itemIndex];
        
                    if (currentItem == -1) continue;
                    if (indexGrid[itemIndex + width] != currentItem ||
                        indexGrid[itemIndex + 2 * width] != currentItem) continue;
                    
                    for (int i = 0; i < 3; i++)
                    {
                        matchedItems.Add(new Vector2Int(x, y + i));
                    }
                }
            }
        
            var matchedItemsTuple = matchedItems.Distinct().ToList();
        
            return matchedItemsTuple;
        }
        
        private async ValueTask DestroyMatchedItems(Board board)
        {
            var itemGrid = board.ItemGrid;
            var indexGrid = board.IndexGrid;
            var width = BoardSettings.Width;
        
            _tasks.Clear();
            foreach (var item in BoardStateController.MatchedItems)
            {
                var gridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(item.x, item.y, width);
                
                if (itemGrid[gridIndex] is not ICollectible boardItem) continue;
        
                var id = indexGrid[gridIndex];
                _tasks.Add(boardItem.CollectItem(drop =>
                {
                    _poolController.ReturnToPool(drop ,id);
                }, _yield));
                
                itemGrid[gridIndex] = null;
                indexGrid[gridIndex] = -1;
            }
            await Task.WhenAll(_tasks);
        }
    }
}
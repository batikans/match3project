using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Gameplay;
using UnityEngine;

namespace Management
{
    public class ItemSwapState : BoardState
    {
        protected internal ItemSwapState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _matchController = ControllersManager.MatchController;
            _inputController = ControllersManager.InputController;
            _tasks = new Task[2];
        }

        private readonly Task[] _tasks;
        private readonly MatchController _matchController;
        private readonly InputController _inputController;
        private int _touchStartIndex;
        private int _swipeIndex;

        private readonly YieldAwaitable _yield = Task.Yield();

        protected internal override void EnterState()
        {
            base.EnterState();
            
            ItemSwapLogic();
        }

        private async void ItemSwapLogic()
        {
            var matchExist = GetIsMatchExistOnSwipe();
            await SwapItemsOnBoard(matchExist);
            
            if (matchExist)
            {
                BoardStateMachine.ChangeState(BoardStateController.DestroyItemsState);
            }
            else
            {
                BoardStateMachine.ChangeState(BoardStateController.InputReceiveState);
            }
        }
        
        private bool GetIsMatchExistOnSwipe()
        {
            _touchStartIndex = _inputController.TouchStartGridIndex;
            _swipeIndex = _inputController.SwipeGridIndex;
            
            var board = BoardStateController.GameBoard;
            BoardUtilities.SwapItemsOnBoard(board, _touchStartIndex, _swipeIndex);
            
            return _matchController.GetIsAnyMatchExistsOnBoard();
        }

        private async ValueTask SwapItemsOnBoard(bool matchExist)
        {
            var board = BoardStateController.GameBoard;
            
            if (!matchExist) BoardUtilities.SwapItemsOnBoard(board, _touchStartIndex, _swipeIndex);
            
            await SwapAnimationOnItems(matchExist);
        }
        
        private async ValueTask SwapAnimationOnItems(bool isMatch)
        {
            var board = BoardStateController.GameBoard;
            var itemGrid = board.ItemGrid;
            
            var item1 = itemGrid[_touchStartIndex];
            var item2 = itemGrid[_swipeIndex];
        
            var position1 = itemGrid[_touchStartIndex].transform.position;
            var position2 = itemGrid[_swipeIndex].transform.position;

            if (item1 is IMovable movableItem1 && item2 is IMovable movableItem2)
            {
                _tasks[0] = MoveItemToPosition(movableItem1, position2, isMatch);
                _tasks[1] = MoveItemToPosition(movableItem2, position1, isMatch);
        
                await Task.WhenAll(_tasks);
            }
        }
        
        private  async Task MoveItemToPosition(IMovable movable, Vector3 targetPosition, bool isMatch)
        {
            var transformToUse = movable.GetTransform();
            var initialPosition = transformToUse.position;
            var distanceToTarget = Vector3.Distance(initialPosition, targetPosition);
            var duration = 0.16f * distanceToTarget;
        
            if (!isMatch)
            {
                await movable.MoveToPosition(targetPosition, duration * 0.6f, _yield);
                await movable.MoveToPosition(initialPosition, duration * 0.6f, _yield);
            }
            else
            {
                await movable.MoveToPosition(targetPosition, duration, _yield);
            }
        }
    }
}
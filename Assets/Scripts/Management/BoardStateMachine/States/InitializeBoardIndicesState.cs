using System.Collections.Generic;

namespace Management
{
    public class InitializeBoardIndicesState : BoardState
    {
        protected internal InitializeBoardIndicesState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _matchController = ControllersManager.MatchController;
        }

        private readonly MatchController _matchController;

        protected internal override void EnterState()
        {
            base.EnterState();
            
            InitializeBoardIndicesLogic();
        }
        
        private void InitializeBoardIndicesLogic()
        {
            var board = BoardStateController.GameBoard;
            var totalDropCount = BoardSettings.DropReferences.Length;
            var indexGrid = board.IndexGrid;
            
            SetGridSafeZoneIndices(board.Width, board.Height, indexGrid, totalDropCount);
            do
            {
                SetGridCrucialZoneIndices(board.Width, board.Height, indexGrid, totalDropCount);
            } while (_matchController.GetIsAnyMatchExistsOnBoard() ||
                     !_matchController.GetIsAnyPotentialMatchExist());
            
            BoardStateMachine.ChangeState(BoardStateController.InitializeBoardItemsState);
        }
        
        private void SetGridSafeZoneIndices(int width, int height, int[] grid, int totalDropCount)
        {
            for (int x = 0; x < width; x++)
            {
                if ((x + 1) % 3 == 0) continue;
                for (int y = 0; y < height; y++)
                {
                    if ((y + 1) % 3 == 0) continue;
                    grid[BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width)] = 
                        BoardUtilities.GetRandomDropIndex(totalDropCount);
                }
            }
            for (int x = 3 - 1; x < width; x += 3)
            {
                for (int y = 3 - 1; y < height; y += 3)
                {
                    grid[BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width)] = 
                        BoardUtilities.GetRandomDropIndex(totalDropCount);
                }
            }
        }
        
        private void SetGridCrucialZoneIndices(int width, int height, IList<int> grid, int totalDropCount)
        {
            for (int x = 3 - 1; x < width; x += 3)
            {
                for (int y = 0; y < height; y++)
                {
                    if ((y + 1) % 3 == 0) continue;
                    grid[BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width)] = 
                        BoardUtilities.GetRandomDropIndex(totalDropCount);
                }
            }
        
            for (int y = 3 - 1; y < height; y += 3)
            {
                for (int x = 0; x < width; x++)
                {
                    if ((x + 1) % 3 == 0) continue;
                    grid[BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width)] = 
                        BoardUtilities.GetRandomDropIndex(totalDropCount);
                }
            }
        }
    }
}
namespace Management
{
    public class InputReceiveState : BoardState
    {
        protected internal InputReceiveState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
        }

        protected internal override void EnterState()
        {
            base.EnterState();
            ControllersManager.InputController.ToggleInput(true);
        }

        protected internal override void ExitState()
        {
            base.ExitState();
            ControllersManager.InputController.ToggleInput(false);
        }
    }
}
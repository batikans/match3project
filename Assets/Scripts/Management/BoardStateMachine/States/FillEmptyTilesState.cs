using System.Collections.Generic;
using Gameplay;
using UnityEngine;

namespace Management
{
    public class FillEmptyTilesState : BoardState
    {
        protected internal FillEmptyTilesState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _poolController = ControllersManager.PoolController;
            _numOfMissingTilesForEachColumn = new Dictionary<int, int>();
        }

        private readonly PoolController _poolController;
        private readonly Dictionary<int, int> _numOfMissingTilesForEachColumn;

        protected internal override void EnterState()
        {
            base.EnterState();
            
            FillEmptyTilesLogic();
        }
        
        private void FillEmptyTilesLogic()
        {
            SetNumOfMissingTilesForEachColumn();
            var board = BoardStateController.GameBoard;
            
            ReOrderBoardIndices(board);
            
            foreach (var kvp in _numOfMissingTilesForEachColumn)
            {
                if (BoardStateController.ActiveSpawnerIndices.Contains(kvp.Key))
                {
                    SetIndicesForSpawnedColumns(board, kvp.Key, kvp.Value,
                        BoardStateController.TotalDropsCount);
                }
            }
            
            BoardStateMachine.ChangeState(BoardStateController.MoveItemsState);
        }
        
        private void SetNumOfMissingTilesForEachColumn()
        {
            _numOfMissingTilesForEachColumn.Clear();
            
            foreach (var tile in BoardStateController.MatchedItems)
            {
                var columnIndex = tile.x;
            
                if (_numOfMissingTilesForEachColumn.TryGetValue(columnIndex, out var amount))
                {
                    amount++;
                    _numOfMissingTilesForEachColumn[columnIndex] = amount;
                }
                else
                {
                    _numOfMissingTilesForEachColumn[columnIndex] = 1;
                }
            }
        }
        
        private void ReOrderBoardIndices(Board board)
        {
            var height = board.Height;
            var width = board.Width;
        
            foreach (var kvp in _numOfMissingTilesForEachColumn)
            {
                var gridToCheck = board.IndexGrid;
            
                var noEmptyBelow = true;
                while (noEmptyBelow)
                {
                    noEmptyBelow = false;
                    for (int y = height - 1; y > 0; y--)
                    {
                        var gridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(kvp.Key, y, width);
                        var gridIndexBelow = BoardUtilities.GetGridIndexBasedOnCoordinates(kvp.Key, y - 1, width);
                    
                        if (gridToCheck[gridIndex] != -1 && gridToCheck[gridIndexBelow] == -1)
                        {
                            noEmptyBelow = true;
                            BoardUtilities.SwapItemsOnBoard(board, gridIndex, gridIndexBelow);
                        }
                    }
                }
            }
        }
        
        private void SetIndicesForSpawnedColumns(Board board, int columnIndex, int spawnAmount, int totalDropsCount)
        {
            var grid = board.IndexGrid;
            var width = board.Width;
            var height = board.Height;
            var initialHeightIndex = height - 1;
        
            for (int y = height - 1; y > initialHeightIndex - spawnAmount; y--)
            {
                grid[BoardUtilities.GetGridIndexBasedOnCoordinates(columnIndex, y, width)]
                    = BoardUtilities.GetRandomDropIndex(totalDropsCount);
            }
        
            SpawnDropsOverTheBoard(board, columnIndex, spawnAmount);
        }
        
        private void SpawnDropsOverTheBoard(Board board, int columnIndex, int amount)
        {
            var height = board.Height;
            var width = board.Width;
            var indexGrid = board.IndexGrid;
            var itemGrid = board.ItemGrid;
        
            for (int i = 0; i < amount; i++)
            {
                var y = (height + i) - amount;
                var gridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(columnIndex, y, width);
                var dropIndex = indexGrid[gridIndex];
                
                var spawnedItem = _poolController.GetFromPool(dropIndex);
                spawnedItem.transform.position = new Vector3(columnIndex, height + i, 0);
            
                itemGrid[gridIndex] = spawnedItem;
            }
        }
    }
}
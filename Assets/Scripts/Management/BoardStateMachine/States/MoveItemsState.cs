using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Gameplay;
using UnityEngine;

namespace Management
{
    public class MoveItemsState : BoardState
    {
        protected internal MoveItemsState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _matchController = ControllersManager.MatchController;
            
            _tasks = new List<Task>();
            _delayTask = Task.Delay(200);
            
            _moveSpeedMultiplier = Mathf.Pow(BoardSettings.MoveSpeed, -1);
            _shuffleMoveSpeedMultiplier = Mathf.Pow(BoardSettings.ShuffleMoveSpeed, -1);
        }

        private readonly MatchController _matchController;
        private readonly List<Task> _tasks;
        private readonly Task _delayTask;
        private readonly float _moveSpeedMultiplier;
        private readonly float _shuffleMoveSpeedMultiplier;

        private readonly YieldAwaitable _yield = Task.Yield();

        protected internal override void EnterState()
        {
            base.EnterState();
            
            MoveItemsLogic();
        }
        
        private async void MoveItemsLogic()
        {
            var board = BoardStateController.GameBoard;
        
            if (BoardStateMachine.LastState == BoardStateController.ShuffleBoardState)
            {
                await MoveItems(board, _shuffleMoveSpeedMultiplier);
                BoardStateMachine.ChangeState(BoardStateController.InputReceiveState);
                return;
            }
            
            await MoveItems(board, _moveSpeedMultiplier);
            await _delayTask;
            
            if (!_matchController.GetIsAnyPotentialMatchExist())
            {
                BoardStateMachine.ChangeState(BoardStateController.ShuffleBoardState);
            }
            else
            {
                if (!_matchController.GetIsAnyMatchExistsOnBoard())
                {
                    BoardStateMachine.ChangeState(BoardStateController.InputReceiveState);
                }
                else
                {
                    BoardStateMachine.ChangeState(BoardStateController.DestroyItemsState);
                }
            }
        }
        
        private async ValueTask MoveItems(Board board, float speedMultiplier)
        {
            var width = board.Width;
            var height = board.Height;
            var itemGrid = board.ItemGrid;

            _tasks.Clear();
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var itemToFall = 
                        itemGrid[BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width)];

                    if (itemToFall is not IMovable movableItem) continue;
                    
                    var itemPosition = itemToFall.transform.position;
                    var targetPosition = new Vector3(x, y, 0);
                    var distanceToTarget = Vector2.Distance(itemPosition, targetPosition);

                    if (distanceToTarget < 0.3f) continue;
                        
                    _tasks.Add(movableItem.MoveToPosition(targetPosition, distanceToTarget * speedMultiplier, _yield));
                }
            }
            await Task.WhenAll(_tasks);
        }
    }
}
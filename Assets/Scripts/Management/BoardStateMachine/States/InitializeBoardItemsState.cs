using UnityEngine;

namespace Management
{
    public class InitializeBoardItemsState : BoardState
    {
        protected internal InitializeBoardItemsState(BoardStateController boardStateController) 
            : base(boardStateController)
        {
            _poolController = ControllersManager.PoolController;
        }

        private readonly PoolController _poolController;
        
        protected internal override void EnterState()
        {
            base.EnterState();
            
            InitializeBoardItemsLogic();
        }

        private void InitializeBoardItemsLogic()
        {
            FillBoard();
            
            BoardStateMachine.ChangeState(BoardStateController.InputReceiveState);
        }
        
        private void FillBoard()
        {
            var board = BoardStateController.GameBoard;
            var height = board.Height;
            var width = board.Width;
            var indexGrid = board.IndexGrid;
            var itemGrid = board.ItemGrid;
        
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var gridIndex = BoardUtilities.GetGridIndexBasedOnCoordinates(x, y, width);
                    var dropIndex = indexGrid[gridIndex];
                    var spawnedItem = _poolController.GetFromPool(dropIndex);
                    
                    spawnedItem.transform.position = new Vector3(x, y, 0);

                    itemGrid[gridIndex] = spawnedItem;
                }
            }
        }
    }
}
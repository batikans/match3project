namespace Management
{
    public class BoardState
    {
        protected readonly BoardStateController BoardStateController;
        protected readonly BoardStateMachine BoardStateMachine;
        protected readonly BoardSettings BoardSettings;
        protected readonly ControllersManager ControllersManager;
    
        protected BoardState(BoardStateController boardStateController)
        {
            BoardStateController = boardStateController;
            BoardStateMachine = BoardStateController.BoardStateMachine;
            BoardSettings = BoardStateController.BoardSettings;
            ControllersManager = BoardStateController.ControllersManager;
        }

        protected internal virtual void EnterState()
        {
        }

        protected internal virtual void ExitState()
        {
        }
    }
}

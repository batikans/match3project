namespace Management
{
    public class BoardStateMachine
    {
        private BoardState CurrentState { get; set; }
        internal BoardState LastState { get; private set; }

        public void Initialize(BoardState startingState)
        {
            CurrentState = startingState;
            LastState = null;
            CurrentState.EnterState();
        }
        
        public void ChangeState(BoardState newState)
        {
            CurrentState.ExitState();
            LastState = CurrentState;
            CurrentState = newState;
            CurrentState.EnterState();
        }
    }
}